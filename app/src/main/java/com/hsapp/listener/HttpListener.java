package com.hsapp.listener;

/**
 * Created by nbzl on 2017/7/27.
 */
public interface HttpListener {
    void success(Object obj);
    void fail(String msg);
}
