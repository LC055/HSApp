package com.hsapp.listener;

/**
 * Created by nbzl on 2017/8/21.
 */
public interface UpFileListener {
    public void start();
    public void success(Object obj);
    public void failure(String msg);
}
