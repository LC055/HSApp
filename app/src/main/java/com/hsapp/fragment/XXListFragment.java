package com.hsapp.fragment;

import android.util.Log;
import android.view.LayoutInflater;

import com.hsapp.listener.LoadStatusListener;
import com.hsapp.model.XXListModel;

/**
 * Created by apple on 2018/1/26.
 */

public class XXListFragment extends BaseFragment{
    private XXListModel model;

    @Override
    protected void initContent(LayoutInflater inflater) {
        super.initContent(inflater);

        model=new XXListModel(getActivity(),new LoadStatusImpl());
    }

    @Override
    protected void afterview() {
        super.afterview();
        //AddBaseContentView(loadLayoutModel.getBinding().getRoot());
    }

    @Override
    protected void loaddata() {
        Log.e("Mytext","XXListFragment-->loaddata");
        AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        model.postRequest();
    }


    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {
            AddBaseContentView(model.getBinding().getRoot());
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddBaseContentView(loadLayoutModel.getBinding().getRoot());
        }
    }


}
