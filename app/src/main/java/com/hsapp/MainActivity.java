package com.hsapp;

import android.app.ActivityOptions;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.hsapp.activity.GZRYDetailActivity;
import com.hsapp.activity.HomeActivity;
import com.hsapp.databinding.ActivityMainBinding;
import com.hsapp.util.PreferencesUtil;
import com.hsapp.util.Static;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private SpringAnimation animation;
    private Intent startintent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=DataBindingUtil.setContentView(MainActivity.this,R.layout.activity_main);
        startintent=new Intent(MainActivity.this, HomeActivity.class);

        if(getIntent()==null||getIntent().getStringExtra("description")==null){
            SpringForce spring = new SpringForce(0)
                    .setDampingRatio(SpringForce.DAMPING_RATIO_LOW_BOUNCY)
                    .setStiffness(SpringForce.STIFFNESS_VERY_LOW);
            animation=new SpringAnimation(binding.logoimg, DynamicAnimation.TRANSLATION_Y).setSpring(spring).setStartValue(-700);

            animation.addEndListener(new DynamicAnimation.OnAnimationEndListener() {
                @Override
                public void onAnimationEnd(DynamicAnimation animation, boolean canceled, float value, float velocity) {
                    turn();
                    //text();
                }
            });
            animation.start();
        }else{
            String description=getIntent().getStringExtra("description");
            String id=description.substring(description.lastIndexOf(",")+1,description.length());
            String code=getIntent().getStringExtra("POLICE_STORE");
            Log.e("id",code+"<---->"+id);
            Intent intent=new Intent(MainActivity.this,GZRYDetailActivity.class);
            intent.putExtra("id",id);
            startActivity(intent);
            finish();
        }


    }

    public void turn(){
        PreferencesUtil.getInstance(MainActivity.this).setLastLoginPoliceCode(getIntent().getExtras().getString("POLICE_STORE"));
        PreferencesUtil.getInstance(MainActivity.this).setLoginSFZH(getIntent().getExtras().getString("SFZH"));
        int x=(int)binding.logoimg.getTranslationX();
        int y=(int)binding.logoimg.getTranslationY();
        if(android.os.Build.VERSION.SDK_INT>=23){
            startActivity(startintent, ActivityOptions.makeClipRevealAnimation(binding.logoimg,x,y,binding.logoimg.getWidth(),binding.logoimg.getHeight()).toBundle());
        }else{
            startActivity(startintent);
        }

        finish();
    }


    public void text(){
            PreferencesUtil.getInstance(MainActivity.this).setLastLoginPoliceCode(Static.policecode);
            PreferencesUtil.getInstance(MainActivity.this).setLoginSFZH(Static.sfzh);
            int x=(int)binding.logoimg.getTranslationX();
            int y=(int)binding.logoimg.getTranslationY();
            if(android.os.Build.VERSION.SDK_INT>=23){
                startActivity(startintent, ActivityOptions.makeClipRevealAnimation(binding.logoimg,x,y,binding.logoimg.getWidth(),binding.logoimg.getHeight()).toBundle());
            }else{
                startActivity(startintent);
            }



        finish();
    }
}
