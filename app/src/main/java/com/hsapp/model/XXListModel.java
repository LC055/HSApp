package com.hsapp.model;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;

import com.hsapp.R;
import com.hsapp.activity.GZRYDetailActivity;
import com.hsapp.activity.TextActivity;
import com.hsapp.adapter.XXAdapter;
import com.hsapp.bean.GzryBean;
import com.hsapp.bean.GzryListBean;
import com.hsapp.databinding.ActivityListBinding;
import com.hsapp.http.MNetHttp;
import com.hsapp.listener.HttpListener;
import com.hsapp.listener.LoadStatusListener;
import com.hsapp.listener.RecyclerItemClick;
import com.hsapp.listener.RefreshListener;
import com.hsapp.util.PreferencesUtil;
import com.hsapp.util.RefreshUtil;
import com.hsapp.util.Static;
import com.hsapp.widget.RVItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 2018/1/29.
 */

public class XXListModel extends BaseModel{
    private LoadStatusListener statusListener;
    private ActivityListBinding binding;
    private Map<String,String> params;
    private List<Object> datalist;
    private XXAdapter adapter;
    private RefreshUtil refreshUtil;
    private Intent intent;
    private int page=1;

    public XXListModel(Context context,LoadStatusListener statusListener){
        super(context);
        this.statusListener=statusListener;
        params=new HashMap<String,String>();
        datalist=new ArrayList<Object>();

        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_list,null,false);
        refreshUtil=new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();
    }

    public ActivityListBinding getBinding(){
        return binding;
    }



    public void postRequest(){
        params.put("_page",page+"");
        params.put("_row",Static.rows);
        params.put("fPolice_No", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());

        MNetHttp.getInstance().rxPostRequest(Static.GZRYLIST,params,GzryListBean.class,new HttpImpl());

    }

    class HttpImpl implements HttpListener{

        @Override
        public void success(Object obj) {
            if(((GzryListBean)obj).getRows()==null||((GzryListBean)obj).getRows().size()==0){
                if(page==1){
                    statusListener.hide(false, "数据为空");
                }else{
                    Static.showToast(context,context.getString(R.string.no_more_data));
                }
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                }, 20);
            }else{
                if(page==1){
                    datalist.clear();
                }
                datalist.addAll(((GzryListBean)obj).getRows());
                initAdapter();
                statusListener.show();
                //Log.e("Mytext","size---->"+((JZListBean)obj).getList().get(1).getId());
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                },20);

            }

        }

        @Override
        public void fail(String msg) {

        }
    }


    public void initAdapter(){
        if(adapter==null){
            adapter=new XXAdapter(context,datalist,R.layout.item_xx, new ItemClick());

            //adapter=new Tab1ItemAdapter(context,datalist,R.layout.item_tab1,null,new EditClick());
            //adapter.setMode(Attributes.Mode.Single);
            binding.recycleview.setAdapter(adapter);
            binding.recycleview.addItemDecoration(new RVItemDecoration(30));
            binding.recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        }else {
            adapter.notifyDataSetChanged();
        }
    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {

            page=1;
            postRequest();
        }

        @Override
        public void loadmore() {


            page++;
            postRequest();
        }
    }

    class ItemClick implements RecyclerItemClick {

        @Override
        public void onItemClick(int position) {
            intent=new Intent(context, GZRYDetailActivity.class);
            intent.putExtra("id",""+((GzryBean)(datalist.get(position))).getZj());
            //intent.putExtra("data",((GzryBean)(datalist.get(position))).getGzry());
            //intent.putExtra("time",((GzryBean)(datalist.get(position))).getGzsj());

            Intent txt=new Intent(context,TextActivity.class);
            context.startActivity(intent);
        }

        @Override
        public void onItemLongClick(int position) {

        }

        @Override
        public void onMoreClick(int position) {

        }
    }
}
