package com.hsapp.model;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;

import com.hsapp.R;
import com.hsapp.adapter.GridlayoutAdapter;
import com.hsapp.bean.GZRYDataBean;
import com.hsapp.bean.GZRYDetailBean;
import com.hsapp.bean.WjsBean;
import com.hsapp.databinding.ActivityGzrydetailBinding;
import com.hsapp.http.MNetHttp;
import com.hsapp.listener.HttpListener;
import com.hsapp.listener.LoadStatusListener;
import com.hsapp.listener.RecyclerItemClick;
import com.hsapp.util.Static;
import com.hsapp.widget.GridItemDecoration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 2018/2/9.
 */

public class GZRYDetailModel {

    private Context context;
    private LoadStatusListener statusListener;
    private String id;
    private GridlayoutAdapter adapter;
    private ActivityGzrydetailBinding binding;

    public GZRYDetailModel(Context context,String id, LoadStatusListener statusListener){

        this.id=id;
        this.context=context;
        this.statusListener=statusListener;
        binding=DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_gzrydetail,null,false);
        binding.recycleview.setLayoutManager(new GridLayoutManager(context,3));
        binding.recycleview.addItemDecoration(new GridItemDecoration(10));
    }

    public ActivityGzrydetailBinding getBinding(){
        return binding;
    }

    public void postRequest(){
        Map<String,String> params = new HashMap<String,String>();
        params.put("zj",id);//Static.id
        MNetHttp.getInstance().rxPostRequest(Static.GZDETAIL,params,GZRYDetailBean.class,new RequestImpl());
    }

    public void setData(GZRYDataBean data,String time){
        binding.setData(data);
        binding.setTime(time);
        initAdapter(data.getWjs());
        statusListener.show();
    }

    class RequestImpl implements HttpListener {

        @Override
        public void success(Object obj) {
            if(((GZRYDetailBean)obj).getGzry()==null){
                Static.showToast(context,"无数据");
                statusListener.hide(false, "网络异常");
            }else{
                binding.setData(((GZRYDetailBean)obj).getGzry().getGzry());
                initAdapter(((GZRYDetailBean)obj).getGzry().getWjs());
                statusListener.show();
            }

        }

        @Override
        public void fail(String msg) {
            Static.showToast(context,msg);
            statusListener.hide(false, "网络异常");
        }
    }

    private void initAdapter(List<WjsBean> imglist){//WjsBean

        if(adapter==null){
            adapter=new GridlayoutAdapter(context,imglist,R.layout.grid_img,new RecycleClick());
            binding.recycleview.setAdapter(adapter);
        }else{
            adapter.notifyDataSetChanged();
        }
    }

    class RecycleClick implements RecyclerItemClick {

        @Override
        public void onItemClick(int position) {
            //Static.showToast(context,position+"");
        }

        @Override
        public void onItemLongClick(int position) {

        }

        @Override
        public void onMoreClick(int position) {

        }
    }
}
