package com.hsapp.model;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;

import com.hsapp.R;
import com.hsapp.databinding.LayoutLoadBinding;
import com.hsapp.listener.ViewClickListener;

/**
 * Created by nbzl on 2017/8/10.
 */
public class LoadLayoutModel {
    public Activity context;
    public LayoutLoadBinding binding;
    public ViewClickListener listener;
    public static String EMPTY="数据为空";

    public LoadLayoutModel(Activity context, ViewClickListener listener){
        this.context=context;
        this.listener=listener;
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_load,null,false);
        setLoadStatus(true,EMPTY,0);
    }

    public void setLoadStatus(boolean flag,String statusMsg,int drawable){
        binding.setClick(this);
        binding.setIsLoad(flag);
        binding.setStatusTxt(statusMsg);
        if(drawable==0){
            binding.setDrawable(context.getResources().getDrawable(R.drawable.net_fail));
        }else{
            binding.setDrawable(context.getResources().getDrawable(drawable));
        }

    }

    public LayoutLoadBinding getBinding(){
        return binding;
    }

    public void OnItemClick(View view){
        listener.reRequest(view);
    }
}
