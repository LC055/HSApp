package com.hsapp.model;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;

import com.hsapp.R;
import com.hsapp.adapter.ViewPagerAdapter;
import com.hsapp.databinding.ActivityGzrylistBinding;
import com.hsapp.fragment.XXListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 2018/1/29.
 */

public class GZRYListModel {
    private ActivityGzrylistBinding binding;
    private List<Fragment> fragments;
    protected ViewPagerAdapter adapter;

    public GZRYListModel(Context context,FragmentManager manager){
        fragments=new ArrayList<Fragment>();
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_gzrylist,null,false);
        fragments.add(new XXListFragment());
        //fragments.add(new RYListFragment());
        adapter = new ViewPagerAdapter(manager, fragments);
        binding.vpPager.setAdapter(adapter);
        binding.vpPager.setOffscreenPageLimit(1);
    }

    public ActivityGzrylistBinding getBinding(){
        return binding;
    }

}
