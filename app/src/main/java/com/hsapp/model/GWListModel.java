package com.hsapp.model;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;

import com.hsapp.R;
import com.hsapp.activity.GWDetailActivity;
import com.hsapp.adapter.GWAdapter;
import com.hsapp.bean.GWBean;
import com.hsapp.bean.GWListBean;
import com.hsapp.databinding.ActivityGwlistBinding;
import com.hsapp.http.MNetHttp;
import com.hsapp.listener.HttpListener;
import com.hsapp.listener.LoadStatusListener;
import com.hsapp.listener.RecyclerItemClick;
import com.hsapp.listener.RefreshListener;
import com.hsapp.util.RefreshUtil;
import com.hsapp.util.Static;
import com.hsapp.widget.RVItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 2018/1/24.
 */

public class GWListModel {
    private int page=1;
    private Context context;
    private List<Object> datalist;
    private Map<String,String> params;
    private LoadStatusListener statusListener;
    private RefreshUtil refreshUtil;
    protected ActivityGwlistBinding binding;
    private GWAdapter adapter;
    private Intent intent;

    public GWListModel(Context context,LoadStatusListener statusListener){
        super();
        this.context=context;
        this.statusListener=statusListener;
        datalist=new ArrayList<Object>();
        params=new HashMap<String,String>();
        intent=new Intent(context, GWDetailActivity.class);
        binding = DataBindingUtil.inflate(LayoutInflater.from(context),R.layout.activity_gwlist,null,false);
        refreshUtil=new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();
        statusListener.hide(true, "");
        postRequest();
    }

    public ActivityGwlistBinding getBinding(){
        return binding;
    }

    public void initAdapter(){
        if(adapter==null){
            adapter=new GWAdapter(datalist,R.layout.item_gwlist, new ItemClick());

            //adapter=new Tab1ItemAdapter(context,datalist,R.layout.item_tab1,null,new EditClick());
            //adapter.setMode(Attributes.Mode.Single);
            binding.recycleview.setAdapter(adapter);
            binding.recycleview.addItemDecoration(new RVItemDecoration(30));
            binding.recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

        }else {
            adapter.notifyDataSetChanged();
        }
    }


    private void postRequest(){

        Map<String,String> param=new HashMap<String,String>();
        //param.put("fPolice_No", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
        param.put("fPolice_No",Static.policecode);
        param.put("page",page+"");
        param.put("rows",Static.rows);
        MNetHttp.getInstance().rxPostRequest(Static.GWList,param,GWListBean.class,new ListRequestListener());

    }

    class ListRequestListener implements HttpListener {

        @Override
        public void success(Object obj) {
            if(((GWListBean)obj).getRows()==null||((GWListBean)obj).getRows().size()==0){
                if(page==1){
                    statusListener.hide(false, "数据为空");
                }else{
                    Static.showToast(context,context.getString(R.string.no_more_data));
                }
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                }, 20);
            }else{
                if(page==1){
                    datalist.clear();
                }
                datalist.addAll(((GWListBean)obj).getRows());
                initAdapter();
                statusListener.show();
                //Log.e("Mytext","size---->"+((JZListBean)obj).getList().get(1).getId());
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                },20);

            }
        }

        @Override
        public void fail(String msg) {

        }

    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {

            page=1;
            postRequest();
        }

        @Override
        public void loadmore() {


            page++;
            postRequest();
        }
    }

    class ItemClick implements RecyclerItemClick {

        @Override
        public void onItemClick(int position) {
            Static.GWDetail = (GWBean) datalist.get(position);
            context.startActivity(intent);
        }

        @Override
        public void onItemLongClick(int position) {

        }

        @Override
        public void onMoreClick(int position) {

        }
    }
}
