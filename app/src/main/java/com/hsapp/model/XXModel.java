package com.hsapp.model;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;

import com.hsapp.R;
import com.hsapp.databinding.ActivityListBinding;
import com.hsapp.listener.LoadStatusListener;
import com.hsapp.listener.RefreshListener;
import com.hsapp.util.RefreshUtil;

/**
 * Created by apple on 2018/1/26.
 */

public class XXModel {
    private Context context;
    private LoadStatusListener statusListener;
    private RefreshUtil refreshUtil;
    private int page=1;
    private ActivityListBinding binding;
    public XXModel(Context context, LoadStatusListener statusListener) {
        this.context=context;
        this.statusListener=statusListener;
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_list,null,false);
        refreshUtil = new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();
    }

    public ActivityListBinding getBinding(){

        return binding;
    }

    public void postRequest(){

    }

    public void reloadRequest(){
        page=1;
        postRequest();
    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {

            page=1;
            postRequest();
        }

        @Override
        public void loadmore() {


            page++;
            postRequest();
        }
    }
}
