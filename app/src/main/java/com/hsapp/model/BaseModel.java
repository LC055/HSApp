package com.hsapp.model;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by nbzl on 2017/8/11.
 */
public abstract class BaseModel {

    protected ProgressDialog progressDialog;
    protected Context context;

    public BaseModel(Context context){
        this.context=context;
        progressDialog=new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("正在加载中");
    }

}
