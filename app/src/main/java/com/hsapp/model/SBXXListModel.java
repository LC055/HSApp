package com.hsapp.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;

import com.hsapp.R;
import com.hsapp.activity.SBDetailActivity;
import com.hsapp.adapter.SBAdapter;
import com.hsapp.bean.SBDetailBean;
import com.hsapp.bean.SBListBean;
import com.hsapp.databinding.ActivityGwlistBinding;
import com.hsapp.http.MNetHttp;
import com.hsapp.listener.HttpListener;
import com.hsapp.listener.LoadStatusListener;
import com.hsapp.listener.RecyclerItemClick;
import com.hsapp.listener.RefreshListener;
import com.hsapp.util.PreferencesUtil;
import com.hsapp.util.RefreshUtil;
import com.hsapp.util.Static;
import com.hsapp.widget.RVItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by apple on 2018/3/1.
 */

public class SBXXListModel extends BaseModel{
    private ActivityGwlistBinding binding;
    private RefreshUtil refreshUtil;
    private int page=1;
    private LoadStatusListener statusListener;
    private SBAdapter adapter;
    private Map<String,String> params;
    private List<Object> datalist;
    private Intent intent;
    private String sjly="",content="";

    public SBXXListModel(Context context,String sjly,LoadStatusListener statusListener){
        super(context);
        this.statusListener=statusListener;
        this.sjly=sjly;
        params=new HashMap<String,String>();
        datalist=new ArrayList<Object>();
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_gwlist,null,false);
        refreshUtil=new RefreshUtil(context, binding.ptrLayout, new Refresh());
        refreshUtil.initRefreshView();
        statusListener.hide(true, "");
        intent=new Intent(context, SBDetailActivity.class);
        postRequest();
    }

    public ActivityGwlistBinding getBinding(){
        return binding;
    }

    public void selectType(String type){
        statusListener.hide(true, "");
        page=1;
        sjly=type;
        postRequest();
    }

    public void reload(){
        statusListener.hide(true, "");
        page=1;
        postRequest();
    }

    public void reset(){
        this.content="";
        this.sjly="";
        reload();
    }


    public void searchRequest(String content){
        this.content=content;
        sjly="";
        reload();
    }

    private void postRequest(){
        params.put("fPolice_No", PreferencesUtil.getInstance(context).getLastLoginPoliceCode());
        params.put("_page",""+page);
        params.put("_row", Static.rows);
        params.put("sjly",sjly);
        params.put("sbmc",content);
        MNetHttp.getInstance().rxPostRequest(Static.SBLIST,params,SBListBean.class,new HttpResult());
    }
    public void initAdapter() {
        if (adapter == null) {
            adapter=new SBAdapter(datalist,R.layout.item_sblist,new ItemClick());

            binding.recycleview.setAdapter(adapter);
            binding.recycleview.addItemDecoration(new RVItemDecoration(30));
            binding.recycleview.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        }else{
            adapter.notifyDataSetChanged();
        }
    }

    class HttpResult implements HttpListener {

        @Override
        public void success(Object obj) {

            if(((SBListBean)obj).getRows()==null||((SBListBean)obj).getRows().size()==0){
                if(page==1){
                    statusListener.hide(false, "数据为空");
                }else{
                    Static.showToast(context,context.getString(R.string.no_more_data));
                }
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                }, 20);
            }else{
                if(page==1){
                    datalist.clear();
                }
                datalist.addAll(((SBListBean)obj).getRows());
                initAdapter();
                statusListener.show();
                //Log.e("Mytext","size---->"+((JZListBean)obj).getList().get(1).getId());
                binding.ptrLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ptrLayout.refreshComplete();
                    }
                },20);

            }

        }

        @Override
        public void fail(String msg) {

        }
    }

    class Refresh implements RefreshListener {

        @Override
        public void refresh() {
            page=1;
            postRequest();
        }

        @Override
        public void loadmore() {


            page++;
            postRequest();
        }
    }

    class ItemClick implements RecyclerItemClick {

        @Override
        public void onItemClick(int position) {
            intent.putExtra("data",((SBDetailBean)(datalist.get(position))));
            ((Activity)context).startActivityForResult(intent,100);
            //context.startActivity(intent);
        }

        @Override
        public void onItemLongClick(int position) {

        }

        @Override
        public void onMoreClick(int position) {

        }
    }


}
