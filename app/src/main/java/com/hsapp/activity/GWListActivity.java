package com.hsapp.activity;

import com.hsapp.listener.LoadStatusListener;
import com.hsapp.model.GWListModel;

/**
 * Created by apple on 2018/1/24.
 */

public class GWListActivity extends BaseActivity{
    private GWListModel model;

    @Override
    protected void after() {
        handerBackToolar("公文推送");
        model=new GWListModel(this,new LoadStatusImpl());
    }


    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {
            AddContentView(model.getBinding().getRoot());
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddContentView(loadLayoutModel.getBinding().getRoot());
        }
    }
}
