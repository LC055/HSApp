package com.hsapp.activity;

import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.hsapp.R;
import com.hsapp.databinding.ActivityHomeBinding;
import com.hsapp.util.PreferencesUtil;

/**
 * Created by lc on 2018/1/23.
 */
public class HomeActivity extends BaseActivity {
    private ActivityHomeBinding binding;

    @Override
    protected void after() {
        Log.e("Mytext","--->"+PreferencesUtil.getInstance(HomeActivity.this).getLastLoginPoliceCode());
        baseBinding.toolbar.setLogo(ContextCompat.getDrawable(HomeActivity.this,R.mipmap.znfk));
        baseBinding.toolbar.setContentInsetsAbsolute(50,0);
        baseBinding.toolbar.setTitle(R.string.app_name);
        baseBinding.toolbar.setTitleMargin(60,0,10,0);
        binding= DataBindingUtil.inflate(LayoutInflater.from(HomeActivity.this), R.layout.activity_home,null,false);
        binding.setClick(new ViewClick());
        AddContentView(binding.getRoot());
    }


    public class ViewClick {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tab1btn:
                    jumpToActivity(GWListActivity.class);
                    break;
                case R.id.tab2btn:
                    jumpToActivity(FileUpActivity.class);
                    break;
                case R.id.tab3btn:
                    jumpToActivity(GZRYListActivity.class);
                    break;
                case R.id.tab4btn:
                    jumpToActivity(SBXXListActivity.class);
                    break;
            }
        }
    }
}
