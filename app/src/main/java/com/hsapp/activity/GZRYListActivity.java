package com.hsapp.activity;

import android.support.design.widget.TabLayout;
import android.view.View;

import com.hsapp.model.GZRYListModel;

/**
 * Created by apple on 2018/1/26.
 */

public class GZRYListActivity extends BaseActivity{

    private GZRYListModel model;

    @Override
    protected void after() {
        model=new GZRYListModel(GZRYListActivity.this,getSupportFragmentManager());
        baseBinding.tabs.setVisibility(View.VISIBLE);
        baseBinding.tabs.setTabMode(TabLayout.MODE_FIXED);
        handerBackToolar("返回");

        //baseBinding.tabs.addTab(baseBinding.tabs.newTab().setText("人员"));
        //baseBinding.tabs.addTab(baseBinding.tabs.newTab().setText("消息"));
        baseBinding.tabs.setupWithViewPager(model.getBinding().vpPager);
        //baseBinding.tabs.getTabAt(0).setText("人员");
        //baseBinding.tabs.getTabAt(1).setText("消息");
        //baseBinding.tabs.setTabsFromPagerAdapter(model.getAdapter());


        AddContentView(model.getBinding().getRoot());
    }
}
