package com.hsapp.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hsapp.R;
import com.hsapp.listener.LoadStatusListener;
import com.hsapp.model.SBXXListModel;
import com.wyt.searchbox.SearchFragment;
import com.wyt.searchbox.custom.IOnSearchClickListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 2018/3/1.
 */

public class SBXXListActivity extends BaseActivity {

    private View popview;
    private DisplayMetrics dm;
    private SBXXListModel model;
    private PopupWindow popupWindow;
    private TextView all,select1,select2,select3;
    private List<TextView> txtlsit;
    private SearchFragment searchFragment;
    private int screenWidth;
    private String[] type=new String[]{"","8261082D3EF24F098BDEDA425862D41E",
            "96A86194446745638E46CD28A0545486",
    "CA8DAD88C1584B549BF150A38116E945"};

    @Override
    protected void after() {
        super.after();
        EventBus.getDefault().register(this);
        searchFragment = SearchFragment.newInstance();
        handerBackToolar("设备信息");
        WindowManager manager = this.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        screenWidth = outMetrics.widthPixels;
        txtlsit=new ArrayList<TextView>();
        model=new SBXXListModel(SBXXListActivity.this,type[0],new LoadStatusImpl());
        baseBinding.toolbar.setOnMenuItemClickListener(new MenuItemClick());
        initPopView();
        searchFragment.setOnSearchClickListener(new IOnSearchClickListener() {
            @Override
            public void OnSearchClick(String keyword) {
                //这里处理逻辑
                //Toast.makeText(SBXXListActivity.this, keyword, Toast.LENGTH_SHORT).show();
                model.searchRequest(keyword);
                reset(0);
            }
        });
    }
    private void initPopView(){
        popview=getLayoutInflater().inflate(R.layout.layout_sbtype,null);
        all=(TextView)popview.findViewById(R.id.all);
        select1=(TextView)popview.findViewById(R.id.select1);
        select2=(TextView)popview.findViewById(R.id.select2);
        select3=(TextView)popview.findViewById(R.id.select3);
        all.setOnClickListener(new TypeSelectClick());
        select1.setOnClickListener(new TypeSelectClick());
        select2.setOnClickListener(new TypeSelectClick());
        select3.setOnClickListener(new TypeSelectClick());
        popupWindow=new PopupWindow(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(popview);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
        popupWindow.setOutsideTouchable(true);
        popupWindow.setClippingEnabled(true);
        txtlsit.add(all);
        txtlsit.add(select1);
        txtlsit.add(select2);
        txtlsit.add(select3);
        txtlsit.get(0).setBackgroundColor(ContextCompat.getColor(SBXXListActivity.this,R.color.level_three_text));
        txtlsit.get(0).setTextColor(ContextCompat.getColor(SBXXListActivity.this,R.color.white));

    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {
            AddContentView(model.getBinding().getRoot());
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddContentView(loadLayoutModel.getBinding().getRoot());
        }
    }

    class MenuItemClick implements Toolbar.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()){
                case R.id.sbtype:
                    popupWindow.setFocusable(true);
                    if(popupWindow.isShowing()){
                        popupWindow.dismiss();
                    }else{
                        popupWindow.showAsDropDown(baseBinding.toolbar,screenWidth-5,10);
                    }

                    break;
                case R.id.search:
                    searchFragment.show(getSupportFragmentManager(),SearchFragment.TAG);
                    break;
                case R.id.reset:
                    model.reset();
                    reset(0);
                    break;
            }
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sblist,menu);
        return true;
    }

    class TypeSelectClick implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.all:
                    popClick(0);
                    break;
                case R.id.select1:
                    popClick(1);
                    break;
                case R.id.select2:
                    popClick(2);
                    break;
                case R.id.select3:
                    popClick(3);
                    break;
            }
        }
    }

    private void popClick(int position){
        popupWindow.dismiss();
        model.selectType(type[position]);
        reset(position);
    }

    public void reset(int position){
        for(TextView txt:txtlsit){
            txt.setTextColor(ContextCompat.getColor(SBXXListActivity.this,R.color.level_three_text));
            txt.setBackgroundColor(ContextCompat.getColor(SBXXListActivity.this,R.color.white));
        }
        txtlsit.get(position).setBackgroundColor(ContextCompat.getColor(SBXXListActivity.this,R.color.level_three_text));
        txtlsit.get(position).setTextColor(ContextCompat.getColor(SBXXListActivity.this,R.color.white));

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void isRefresh(boolean isRefresh){
        Log.e("Mytext","---->"+isRefresh);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            switch (requestCode){
                case 100:
                    Log.e("Mytxt","刷新");
                    model.reload();
                    break;
            }
        }
    }
}
