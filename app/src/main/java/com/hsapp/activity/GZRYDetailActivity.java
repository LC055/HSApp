package com.hsapp.activity;

import com.hsapp.bean.GZRYDataBean;
import com.hsapp.listener.LoadStatusListener;
import com.hsapp.model.GZRYDetailModel;

/**
 * Created by apple on 2018/2/9.
 */

public class GZRYDetailActivity extends BaseActivity {
    private GZRYDetailModel model;

    @Override
    protected void after() {
        super.after();
        handerBackToolar("返回");
        AddContentView(loadLayoutModel.getBinding().getRoot());
        model=new GZRYDetailModel(GZRYDetailActivity.this,getIntent().getStringExtra("id"),new LoadStatusImpl());
        if(getIntent().getStringExtra("id").equals("-1")){
            model.setData((GZRYDataBean)(getIntent().getSerializableExtra("data")),getIntent().getStringExtra("time"));
        }else{
            model.postRequest();
        }

    }

    class LoadStatusImpl implements LoadStatusListener {

        @Override
        public void show() {
            AddContentView(model.getBinding().getRoot());
        }

        @Override
        public void hide(boolean flag, String msg) {
            loadLayoutModel.setLoadStatus(flag, msg, 0);
            AddContentView(loadLayoutModel.getBinding().getRoot());
        }
    }
}
