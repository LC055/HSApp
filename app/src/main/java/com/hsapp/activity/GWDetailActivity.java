package com.hsapp.activity;

import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;

import com.hsapp.R;
import com.hsapp.bean.FKBean;
import com.hsapp.databinding.ActivityGwdetailBinding;
import com.hsapp.http.MNetHttp;
import com.hsapp.listener.HttpListener;
import com.hsapp.util.BindingUtil;
import com.hsapp.util.Static;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 2018/1/25.
 */

public class GWDetailActivity extends BaseActivity {
    private ActivityGwdetailBinding binding;

    @Override
    protected void after() {
        handerBackToolar("详情");
        binding= DataBindingUtil.inflate(LayoutInflater.from(GWDetailActivity.this), R.layout.activity_gwdetail,null,false);
        AddContentView(binding.getRoot());
        binding.setData(Static.GWDetail);
        binding.setClick(new ViewClick());
        binding.webview.loadDataWithBaseURL(null,Static.GWDetail.getFcontentTxt(),"text/html","utf-8",null);

    }


    public class ViewClick{
        public void onClick(View view){
            switch (view.getId()){
                case R.id.button:
                    if(BindingUtil.isTextViewEmpty(binding.content).equals("")){
                        Static.showToast(GWDetailActivity.this,"请先填写反馈内容");
                    }else{
                        Map<String,String> params=new HashMap<String,String>();
                        params.put("fPolice_No",Static.policecode);
                        //params.put("fPolice_No", PreferencesUtil.getInstance(GWDetailActivity.this).getLastLoginPoliceCode());
                        params.put("fid",Static.GWDetail.getFid());
                        params.put("fReviewContent",binding.content.getText().toString());
                        MNetHttp.getInstance().rxPostRequest(Static.FKURL, params, FKBean.class, new HttpListener() {
                            @Override
                            public void success(Object obj) {
                                if(((FKBean)obj).getType().equals("2")){
                                    Static.showToast(GWDetailActivity.this,"反馈成功！");
                                    finish();
                                }else{
                                    Static.showToast(GWDetailActivity.this,"后台异常！");
                                }
                            }

                            @Override
                            public void fail(String msg) {
                                Static.showToast(GWDetailActivity.this,msg);
                            }
                        });
                    }


                    break;
            }
        }
    }
}
