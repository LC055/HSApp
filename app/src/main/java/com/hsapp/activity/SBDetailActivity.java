package com.hsapp.activity;

import android.content.Intent;

import com.hsapp.R;
import com.hsapp.listener.ToolbarIconClick;
import com.hsapp.model.SBDetailModel;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by apple on 2018/3/7.
 */

public class SBDetailActivity extends BaseActivity{
    private SBDetailModel model;


    @Override
    protected void after() {
        super.after();
        //EventBus.getDefault().register(this);
        handlerback();

        requestGps();
        model=new SBDetailModel(SBDetailActivity.this);
        AddContentView(model.getBinding().getRoot());
    }

    public void IsEdit(boolean isEdit){
        if(isEdit){
            handerIconToolar("关闭", R.drawable.close, new ToolbarIconClick() {
                @Override
                public void iconClick() {
                    model.closeDialog.show();
                }
            });
        }else{
            handlerback();
        }

    }

    private void handlerback(){
        handerIconToolar("设备详情", R.drawable.backimg, new ToolbarIconClick() {
            @Override
            public void iconClick() {
                if(model.getIsSubmit()){
                    Intent intent=new Intent();
                    setResult(RESULT_OK,intent);
                    //EventBus.getDefault().post(true);
                }
                finish();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void txt(){

    }

    @Override
    public void onBackPressed() {
        if(model.getIsEdit()){
            model.closeDialog.show();
        }else{
            if(model.getIsSubmit()){
                Intent intent=new Intent();
                setResult(RESULT_OK,intent);
                //EventBus.getDefault().post(true);
            }
            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //EventBus.getDefault().unregister(this);
    }
}
