package com.hsapp.widget;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by apple on 2018/1/30.
 */

public class ContentBehavior extends CoordinatorLayout.Behavior<NestedScrollView> {
    private static final String TAG = "ContentBehavior";
    private int offset = 0;
    private int startOffset = 0;
    private int endOffset = 0;
    private Context context;


    public ContentBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View directTargetChild, View target, int nestedScrollAxes) {
        return true;
    }

    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, NestedScrollView child, int layoutDirection) {
        //child.layout(0, (int)context.getResources().getDimension(R.dimen.header_height),parent.getWidth(),parent.getHeight());
        //child.getChildAt(0).layout(0, (int)context.getResources().getDimension(R.dimen.header_height),parent.getWidth(),parent.getHeight());
        View view=child.getChildAt(0);
        return true;
    }


    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, NestedScrollView child, View dependency) {
        return dependency instanceof ImageView;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, NestedScrollView child, View dependency) {
        Log.e("Mytext",dependency.getY()+"<--->");
        return super.onDependentViewChanged(parent, child, dependency);
    }
}
