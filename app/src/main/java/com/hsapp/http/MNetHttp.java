package com.hsapp.http;

import android.support.annotation.NonNull;
import android.util.Log;

import com.hsapp.listener.HttpListener;
import com.hsapp.listener.RxHttpListener;
import com.hsapp.listener.UpFileListener;
import com.hsapp.listener.UpImageListener;
import com.hsapp.util.FileUtil;
import com.hsapp.util.GsonUtil;
import com.hsapp.util.Static;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by nbzl on 2017/7/27.
 */
public class MNetHttp {
    public static MNetHttp mNetHttp;
    public static OkHttpClient client;
    public final static int CONNECT_TIMEOUT = 25 * 1000;
    public final static int READ_TIMEOUT = 25 * 1000;
    public final static int WRITE_TIMEOUT = 25 * 1000;

    public static MNetHttp getInstance() {
        if (mNetHttp == null) {
            mNetHttp = new MNetHttp();
            mNetHttp.client = new OkHttpClient();//.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS).readTimeout(READ_TIMEOUT,TimeUnit.SECONDS).writeTimeout(WRITE_TIMEOUT,TimeUnit.SECONDS).build();
            mNetHttp.client.setConnectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
            mNetHttp.client.setReadTimeout(READ_TIMEOUT, TimeUnit.SECONDS);
            mNetHttp.client.setWriteTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);

            //mNetHttp.client
        }
        return mNetHttp;
    }

    public void postRequest(String url, Map<String, String> map, final HttpListener listener) {
        Request request;
        if (map.isEmpty()) {
            request = new Request.Builder().url(url).build();
        } else {
            MultipartBuilder builder = new MultipartBuilder();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                builder.addFormDataPart(entry.getKey(), entry.getValue());
            }
            request = new Request.Builder().url(url).post(builder.build()).build();
        }


        //   Request
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                listener.fail("网络异常");
            }

            @Override
            public void onResponse(Response response) throws IOException {
                String xml = response.body().string();
                Log.e("Mytext", "-->" + xml);
                listener.success(xml);
            }
        });
    }

    //flag：true (json)
    public void rxPostRequest(final String url, final Map<String, String> datamap, final Class<?> clazz,final HttpListener listener) {
        Log.e("params",GsonUtil.getInstance().getJson(datamap));
        Observable.fromArray(url)
                .subscribeOn(Schedulers.io())
                .map(new Function<String, Map<String, Object>>() {
                    @Override
                    public Map<String, Object> apply(@io.reactivex.annotations.NonNull String s) throws Exception {
                        Map<String, Object> map = new HashMap<String, Object>();
                        try {
                            //Request request;
                            /*if (datamap.isEmpty()) {
                                request = new Request.Builder().url(s).build();
                            } else {
                                MultipartBuilder builder = new MultipartBuilder();
                                for (Map.Entry<String, String> entry : datamap.entrySet()) {
                                    builder.addFormDataPart(entry.getKey(), entry.getValue());
                                }
                                request = new Request.Builder().url(s).post(builder.build()).build();
                            }*/
                            Request request=null;
                            MultipartBuilder builder = new MultipartBuilder();
                            if(!datamap.isEmpty()){
                                for (Map.Entry<String, String> entry : datamap.entrySet()) {
                                    builder.addFormDataPart(entry.getKey(), entry.getValue());
                                }
                                request = new Request.Builder().url(s).post(builder.build()).build();
                            }else{
                                request = new Request.Builder().url(s).build();
                            }


                            Response response = client.newCall(request).execute();
                            String xml = response.body().string();
                            //XmlResult result= XmlResult.newInstance(clazz).fromXml(xml,"detail");
                            Object obj = null;
                            obj = GsonUtil.getInstance().getObject(xml, clazz);
                            Log.e("json", "-->" + xml);
                            map.put("xml", xml);
                            map.put("data", obj);
                        } catch (Exception e) {
                            Log.e("Mytext", "e-->" + e.toString());
                        }
                        return map;
                    }
                })
                .filter(new Predicate<Map<String, Object>>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull Map<String, Object> map) throws Exception {
                        if(map.get("xml")==null||map.get("xml").equals("")){
                            return false;
                        }else{
                            return true;
                        }
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Map<String, Object>>() {
                    @Override
                    public void onNext(@NonNull Map<String, Object> o) {
                        if (o.size() == 0) {
                            listener.fail(Static.NET_ERROR);
                        } else if (o.get("data") == null) {
                            listener.fail(Static.DATA_ERROR);
                        } else {
                            listener.success(o.get("data"));
                        }
                    }

                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        listener.fail(Static.DATA_ERROR);
                    }

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }
                });
    }


    //文件上传
    public void rxUpFile(final String url, final Map<String, String> mapParam, File file, final Class<?> zclass, final UpFileListener view) {
        view.start();
        final List<String> mlist = FileUtil.stringToArray(file);
        int count = mlist.size();
        //Observable.just(new String{}["1","2"]).
        Observable.interval(0, 2, TimeUnit.SECONDS)
                .fromIterable(mlist)//fromIterable
                .subscribeOn(Schedulers.io())
                .map(new Function<String, Map<String, String>>() {

                    @Override
                    public Map<String, String> apply(@io.reactivex.annotations.NonNull String s) throws Exception {
                        Request request;
                        Map<String, String> map = new HashMap<String, String>();
                        Log.e("error", "map-->" + (mlist.indexOf(s) + 1));
                        MultipartBuilder builder = new MultipartBuilder();
                        builder.addFormDataPart("base64", s);
                        builder.addFormDataPart("id", (mlist.indexOf(s) + 1) + "");
                        for (Map.Entry<String, String> entry : mapParam.entrySet()) {
                            builder.addFormDataPart(entry.getKey(), entry.getValue().toString());
                        }
                        if (mlist.indexOf(s) == (mlist.size() - 1)) {
                            map.put("thisend", "1");
                            builder.addFormDataPart("thisend", "1");
                        } else {
                            map.put("thisend", "0");
                            builder.addFormDataPart("thisend", "0");
                        }
                        //RequestBody body=builder.build();

                        request = new Request.Builder().post(builder.build()).url(url).build();
                        Response response = client.newCall(request).execute();
                        String xml = response.body().string();
                        //client.newCall(request).execute()
                        //Log.e("Mytext","--->"+xml);
                        map.put("data", xml);
                        return map;
                    }
                })
                .filter(new Predicate<Map<String, String>>() {
                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull Map<String, String> map) throws Exception {
                        Log.e("error", "filter-->");
                        String s = map.get("data").toString();
                        if (s == null || s.equals("")) {
                            mlist.clear();
                            return false;
                        } else {
                            if (s.contains("200")) {
                                return true;
                            } else {
                                view.failure("后台有误，比对失败");
                                mlist.clear();
                                return false;
                            }

                        }
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Map<String, String>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
//bao shui dongqu renshe fuwu zhongxing
                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Map<String, String> obj) {
                        try {
                            String status = obj.get("thisend").toString();
                            if (status.equals("1")) {
                                Log.e("Mytext", "xml----end--->" + obj.get("data"));
                                Object resultObj = GsonUtil.getInstance().getObject(obj.get("data"), zclass);
                                view.success(resultObj);
                            } else {
                                Log.e("Mytext", "xml----start--->" + obj.get("data"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        Log.e("error", "-->" + e.toString());
                        view.failure("后台有误，比对失败");
                    }

                    @Override
                    public void onComplete() {
                        Log.e("Mytext", "onComplete");
                    }
                });
    }

    public void rxUpBitmap(final String url, final List<String> filepath, final Map<String, String> mapParam, final Class zclass, final UpImageListener listener) {
        listener.start();
        Observable.fromIterable(filepath)
                .subscribeOn(Schedulers.io())
                .map(new Function<String, Map<String, String>>() {

                    @Override
                    public Map<String, String> apply(@io.reactivex.annotations.NonNull String path) throws Exception {
                        File file = new File(path);
                        if (!file.exists()) {
                            return null;
                        } else {
                            Map<String, String> map = new HashMap<String, String>();
                            MultipartBuilder builder = new MultipartBuilder();//.setType(MultipartBody.FORM);
                            //builder.type(MediaType.parse("image/png"));
                            builder.addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("image/png"), file));
                            builder.addFormDataPart("filename", file.getName());
                            for (Map.Entry<String, String> entry : mapParam.entrySet()) {
                                builder.addFormDataPart(entry.getKey(), entry.getValue().toString());
                            }
                            if (filepath.indexOf(path) == (filepath.size() - 1)) {
                                map.put("end", "1");
                            } else {
                                map.put("end", "0");
                            }
                            Request request = new Request.Builder().url(url).post(builder.build()).build();
                            Response response = client.newCall(request).execute();
                            String xml = response.body().string();
                            map.put("data", xml);
                            return map;
                        }

                    }
                })
                .filter(new Predicate<Map<String, String>>() {

                    @Override
                    public boolean test(Map<String, String> map) throws Exception {
                        if (map == null) {
                            listener.failure("上传文件不存在");
                            filepath.clear();
                            return false;
                        } else if (map.get("data") == null || (map.get("data").equals(""))) {
                            listener.failure("上传文件失败");
                            filepath.clear();
                            return false;
                        } else {
                            return true;
                        }

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Map<String, String>>() {

                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull Map<String, String> data) {
                        Log.e("Mytext", "onNext-->" + data.get("data"));
                        Object resultObj = GsonUtil.getInstance().getObject(data.get("data"), zclass);
                        listener.success(resultObj);
                        if (data.get("end").equals("1")) {
                            Log.e("Mytext", "end-->");
                            listener.complete();
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        //listener.failure("");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void rxUpBitmapList(final String url, final List<String> filepath, final Map<String, String> mapParam, final Class zclass, final UpImageListener listener) {
        listener.start();
        Observable.fromArray(url)
                .subscribeOn(Schedulers.io())
                .map(new Function<String, String>() {

                    @Override
                    public String apply(@io.reactivex.annotations.NonNull String path) throws Exception {


                        MultipartBuilder builder = new MultipartBuilder();//.setType(MultipartBody.FORM);
                        //builder.type(MediaType.parse("image/png"));

                        for (int i = 0; i < filepath.size(); i++) {
                            File file = new File(filepath.get(i));
                            if (!file.exists()) {
                                continue;
                            } else {
                                Log.e("file","filepath--->"+filepath.get(i));
                                builder.addFormDataPart("files", file.getName(), RequestBody.create(MediaType.parse("image/png"), file));
                                //builder.addFormDataPart("filename", file.getName());
                            }

                        }

                        for (Map.Entry<String, String> entry : mapParam.entrySet()) {
                            builder.addFormDataPart(entry.getKey(), entry.getValue().toString());
                        }

                        Request request = new Request.Builder().url(url).post(builder.build()).build();
                        Response response = client.newCall(request).execute();
                        String xml = response.body().string();
                        Log.e("Mytext", "xml--->" + xml);
                        return xml;


                    }
                })
                .filter(new Predicate<String>() {

                    @Override
                    public boolean test(String map) throws Exception {
                        if (map == null || map.equals("")) {
                            listener.failure("上传文件失败");
                            filepath.clear();
                            return false;
                        } else {
                            return true;
                        }

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {

                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull String data) {
                        //Object resultObj = GsonUtil.getInstance().getObject(data, zclass);
                        listener.success(data);
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        listener.failure(Static.UPERROR);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void rxListRequest(final List<Map<String, Object>> datamap, final RxHttpListener listener) {
        Observable.fromIterable(datamap)
                .subscribeOn(Schedulers.io())
                .map(new Function<Map<String, Object>, String>() {

                    @Override
                    public String apply(@io.reactivex.annotations.NonNull Map<String, Object> datamap) throws Exception {
                        String url=datamap.get("url").toString();
                        Map<String,String> params=(Map<String, String>) datamap.get("param");
                        MultipartBuilder builder = new MultipartBuilder();
                        for (Map.Entry<String, String> entry : params.entrySet()) {
                            if(entry.getValue()==null){}else if(entry.getValue().equals("无")){}else if(entry.getValue().equals("-1")){}else{
                                //builder.add(entry.getKey(), entry.getValue());
                                builder.addFormDataPart(entry.getKey(),entry.getValue());
                            }

                        }
                        Request request = new Request.Builder().url(url).post(builder.build()).build();
                        Response response = client.newCall(request).execute();
                        String xml = response.body().string();
                        return xml;
                    }
                })
                .filter(new Predicate<String>() {

                    @Override
                    public boolean test(@io.reactivex.annotations.NonNull String s) throws Exception {
                        if (s == null || s.equals("")) {
                            datamap.clear();
                            return false;
                        }
                        return true;

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>(){

                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull String s) {
                        listener.next(s);
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        listener.fail("网络异常,加载失败");
                    }

                    @Override
                    public void onComplete() {
                        listener.finish();
                    }
                });


    }

}
