package com.hsapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hsapp.R;

import java.util.List;

/**
 * Created by lc on 2017/11/24.
 */
public class GridSBAdapter extends BaseAdapter {
    private Context context;
    private List<String> pathlist;
    private boolean isEdit=false;

    public GridSBAdapter(Context context, List<String> pathlist){
        this.context=context;
        this.pathlist=pathlist;
    }


    public void removePosition(int position){
        pathlist.remove(position);
        notifyDataSetChanged();
    }

    public void removeAll(){
        pathlist.clear();
        notifyDataSetChanged();
    }

    public void setIsEdit(boolean isEdit){
        this.isEdit=isEdit;
        notifyDataSetChanged();
    }

    public boolean getIsEdit(){
        return isEdit;
    }

    @Override
    public int getCount() {
        if(isEdit){
            return pathlist.size()+1;
        }else{
            return pathlist.size();
        }

    }

    @Override
    public Object getItem(int position) {
        if (position>(pathlist.size()-1)){
            return null;
        }else{
            return pathlist.get(position);
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.e("item","position--->"+position+"  "+pathlist.size());
        /*final ItemHolder holder;
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.item_img,null);
            holder=new ItemHolder();
            holder.img=(ImageView)convertView.findViewById(R.id.img);
            convertView.setTag(holder);
        }else{
            holder=(ItemHolder)convertView.getTag();
        }*/

        convertView= LayoutInflater.from(context).inflate(R.layout.item_img,null);
        final ItemHolder holder=new ItemHolder();
        holder.img=(ImageView)convertView.findViewById(R.id.img);

        if (position>=pathlist.size()){
            holder.img.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.addbg));
        }else{


            if(pathlist.get(position).contains("http://")){

                Glide.with(context).load(pathlist.get(position)).asBitmap().into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                        holder.img.setImageBitmap(bitmap);
                    }
                });
            }else{
                holder.img.setImageBitmap(BitmapFactory.decodeFile(pathlist.get(position)));
            }
        }

        return convertView;
    }

    class ItemHolder{
        ImageView img;
    }
}
