package com.hsapp.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.hsapp.R;

import java.util.List;

/**
 * Created by lc on 2017/11/24.
 */
public class GridAdapter extends BaseAdapter {
    private Context context;
    private List<String> pathlist;

    public GridAdapter(Context context,List<String> pathlist){
        this.context=context;
        this.pathlist=pathlist;
    }


    public void removePosition(int position){
        pathlist.remove(position);
        notifyDataSetChanged();
    }

    public void removeAll(){
        pathlist.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return pathlist.size();
    }

    @Override
    public Object getItem(int position) {
        return pathlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemHolder holder;
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.item_img,null);
            holder=new ItemHolder();
            holder.img=(ImageView)convertView.findViewById(R.id.img);
            convertView.setTag(holder);
        }else{
            holder=(ItemHolder)convertView.getTag();
        }
        holder.img.setImageBitmap(BitmapFactory.decodeFile(pathlist.get(position)));
        return convertView;
    }

    class ItemHolder{
        ImageView img;
    }
}
