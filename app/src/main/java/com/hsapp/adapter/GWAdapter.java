package com.hsapp.adapter;

import com.hsapp.BR;
import com.hsapp.bean.GWBean;
import com.hsapp.listener.RecyclerItemClick;

import java.util.List;

/**
 * Created by apple on 2018/1/25.
 */

public class GWAdapter extends BaseRcAdapter {
    public GWAdapter(List<Object> datas, int layoutId, RecyclerItemClick listener) {
        super(datas, layoutId, listener);
    }



    public void BinderHolder(BaseRcAdapter.BaseHolder holder, int position) {

        if(holder instanceof ContentHolder){
            holder.getBinding().setVariable(BR.data, ((GWBean)(datas.get(position))));
            holder.getBinding().setVariable(BR.position,""+(position+1));

        }
    }
}
