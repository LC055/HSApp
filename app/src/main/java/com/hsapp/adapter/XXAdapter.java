package com.hsapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.hsapp.BR;
import com.hsapp.R;
import com.hsapp.bean.GzryBean;
import com.hsapp.listener.RecyclerItemClick;

import java.util.List;

/**
 * Created by apple on 2018/1/25.
 */

public class XXAdapter extends BaseRcAdapter {
    private Context context;
    public XXAdapter(Context context,List<Object> datas, int layoutId, RecyclerItemClick listener) {
        super(datas, layoutId, listener);
        this.context=context;
    }



    public void BinderHolder(BaseHolder holder, int position) {

        if(holder instanceof ContentHolder){
            holder.getBinding().setVariable(BR.data, ((GzryBean)(datas.get(position))));
            holder.getBinding().setVariable(BR.position,""+(position+1));

            if(((GzryBean)(datas.get(position))).getGzry().getLytext().equals("在逃")){
                holder.getBinding().setVariable(BR.labelbg, ContextCompat.getDrawable(context,R.drawable.label_red));
            }else if(((GzryBean)(datas.get(position))).getGzry().getLytext().equals("临控")){
                holder.getBinding().setVariable(BR.labelbg, ContextCompat.getDrawable(context,R.drawable.label_yellow));
            }else if(((GzryBean)(datas.get(position))).getGzry().getLytext().equals("前科")){
                holder.getBinding().setVariable(BR.labelbg, ContextCompat.getDrawable(context,R.drawable.label_orgring));
            }else if(((GzryBean)(datas.get(position))).getGzry().getLytext().equals("涉疆")){
                holder.getBinding().setVariable(BR.labelbg, ContextCompat.getDrawable(context,R.drawable.label_blue));
            }else if(((GzryBean)(datas.get(position))).getGzry().getLytext().equals("涉稳")){
                holder.getBinding().setVariable(BR.labelbg, ContextCompat.getDrawable(context,R.drawable.label_blue));
            }

        }
    }
}
