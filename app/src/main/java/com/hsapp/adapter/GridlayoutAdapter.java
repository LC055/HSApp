package com.hsapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hsapp.bean.WjsBean;
import com.hsapp.databinding.GridImgBinding;
import com.hsapp.listener.RecyclerItemClick;
import com.hsapp.util.Static;

import java.util.List;

/**
 * Created by nbzl on 2017/10/24.
 */
public class GridlayoutAdapter<T> extends BaseRcAdapter {
    private Context context;

    public GridlayoutAdapter(Context context, List<T> datas, int layoutId ,RecyclerItemClick listener) {
        this(datas, layoutId, listener);
        this.context=context;
    }

    public GridlayoutAdapter(List<T> datas, int layoutId, RecyclerItemClick listener){
        super(datas,layoutId,listener);
    }


    @Override
    public void BinderHolder(final BaseHolder holder, int position) {
        Log.e("position","--->"+position);
        if (holder instanceof ContentHolder) {
            /*String path = ((Map<String, Object>) datas.get(position)).get("path").toString();
            Object item = ((Map<String, Object>) datas.get(position)).get("data");*/
            if (holder.getBinding() instanceof GridImgBinding) {
                String file = "";
                if(((WjsBean)datas.get(position)).getLy().equals("G_GZRY")){
                    file=Static.imgurl+"ryzp/"+((WjsBean)datas.get(position)).getZj()+((WjsBean)datas.get(position)).getPostfix();
                }else if(((WjsBean)datas.get(position)).getLy().equals("RX_RESULT")){
                    file=Static.imgurl+"gzzp/"+((WjsBean)datas.get(position)).getFilepath()+"/"+((WjsBean)datas.get(position)).getMc();
                }

               // String file = ((WjsBean)datas.get(position)).getZj()+((WjsBean)datas.get(position)).getPostfix();

                Log.e("imgurl","img--->"+file);


                //Glide.with(context).load(Static.imgurl).skipMemoryCache(false).into(((GridImgBinding) holder.getBinding()).imgview);
                Glide.with(context).load(file).asBitmap().into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                        ((GridImgBinding) holder.getBinding()).imgview.setImageBitmap(bitmap);
                    }
                });
                //((GridImgBinding) holder.getBinding()).imgview.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.failimg));
                /*if(path.equals("")){
                    ((GridImgBinding) holder.getBinding()).imgview.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.failimg));
                }else{
                    ((GridImgBinding) holder.getBinding()).imgview.setImageBitmap(BitmapFactory.decodeFile(path));
                }*/

            }

        }
    }
}
