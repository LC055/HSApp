package com.hsapp.bean;

/**
 * Created by apple on 2018/1/31.
 */

public class GzryRyBean {
    private String jlsj;
    private String xm;
    private String sfzh;
    private String hjdz;
    private String lytext;
    private String jctext;
    private String zj;


    public String getJlsj() {
        return jlsj;
    }

    public void setJlsj(String jlsj) {
        this.jlsj = jlsj;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getSfzh() {
        return sfzh;
    }

    public void setSfzh(String sfzh) {
        this.sfzh = sfzh;
    }

    public String getHjdz() {
        return hjdz;
    }

    public void setHjdz(String hjdz) {
        this.hjdz = hjdz;
    }

    public String getLytext() {
        return lytext;
    }

    public void setLytext(String lytext) {
        this.lytext = lytext;
    }

    public String getJctext() {
        return jctext;
    }

    public void setJctext(String jctext) {
        this.jctext = jctext;
    }

    public String getZj() {
        return zj;
    }

    public void setZj(String zj) {
        this.zj = zj;
    }
}
