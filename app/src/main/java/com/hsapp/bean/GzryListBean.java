package com.hsapp.bean;

import java.util.List;

/**
 * Created by apple on 2018/1/31.
 */

public class GzryListBean {
    private String total;
    private List<GzryBean> rows;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<GzryBean> getRows() {
        return rows;
    }

    public void setRows(List<GzryBean> rows) {
        this.rows = rows;
    }
}
