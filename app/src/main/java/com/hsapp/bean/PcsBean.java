package com.hsapp.bean;

/**
 * Created by apple on 2018/1/24.
 */

public class PcsBean {
    private String depshort;
    private String depcode;

    public String getDepshort() {
        return depshort;
    }

    public void setDepshort(String depshort) {
        this.depshort = depshort;
    }

    public String getDepcode() {
        return depcode;
    }

    public void setDepcode(String depcode) {
        this.depcode = depcode;
    }
}
