package com.hsapp.bean;

import java.util.List;

/**
 * Created by apple on 2018/1/25.
 */

public class GWListBean {
    private String total;
    private List<GWBean> rows;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<GWBean> getRows() {
        return rows;
    }

    public void setRows(List<GWBean> rows) {
        this.rows = rows;
    }
}
