package com.hsapp.bean;

/**
 * Created by apple on 2018/1/25.
 */

public class GWBean{
    private String fid;
    private String fdocType;
    private String fdocid;
    private String ftitle;
    private String fdocClsCdName;
    private String fcontentTxt;
    private String xsFReceiveTime;
    private String fName;
    private String fcontentMsg;
    private String fBrief_Name;

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getFdocType() {
        return fdocType;
    }

    public void setFdocType(String fdocType) {
        this.fdocType = fdocType;
    }

    public String getFdocid() {
        return fdocid;
    }

    public void setFdocid(String fdocid) {
        this.fdocid = fdocid;
    }

    public String getFtitle() {
        return ftitle;
    }

    public void setFtitle(String ftitle) {
        this.ftitle = ftitle;
    }

    public String getFdocClsCdName() {
        return fdocClsCdName;
    }

    public void setFdocClsCdName(String fdocClsCdName) {
        this.fdocClsCdName = fdocClsCdName;
    }

    public String getFcontentTxt() {
        return fcontentTxt;
    }

    public void setFcontentTxt(String fcontentTxt) {
        this.fcontentTxt = fcontentTxt;
    }

    public String getXsFReceiveTime() {
        return xsFReceiveTime;
    }

    public void setXsFReceiveTime(String xsFReceiveTime) {
        this.xsFReceiveTime = xsFReceiveTime;
    }

    public String getFName() {
        return fName;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getFcontentMsg() {
        return fcontentMsg;
    }

    public void setFcontentMsg(String fcontentMsg) {
        this.fcontentMsg = fcontentMsg;
    }

    public String getFBrief_Name() {
        return fBrief_Name;
    }

    public void setFBrief_Name(String fBrief_Name) {
        this.fBrief_Name = fBrief_Name;
    }
}
