package com.hsapp.bean;

/**
 * Created by apple on 2018/1/25.
 */

public class DeptDataBean {
    private String depcode;
    private String depname;

    public String getDepcode() {
        return depcode;
    }

    public void setDepcode(String depcode) {
        this.depcode = depcode;
    }

    public String getDepname() {
        return depname;
    }

    public void setDepname(String depname) {
        this.depname = depname;
    }
}
