package com.hsapp.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by apple on 2018/3/7.
 */

public class SBDetailBean implements Serializable{
    private String sbmc;
    private String sbbh;
    private String sblx;
    private String gxdw;
    private String x,y;
    private String zj;
    private List<WjsBean> wjs;


    public String getSbmc() {
        return sbmc;
    }

    public void setSbmc(String sbmc) {
        this.sbmc = sbmc;
    }

    public String getSbbh() {
        return sbbh;
    }

    public void setSbbh(String sbbh) {
        this.sbbh = sbbh;
    }

    public String getSblx() {
        return sblx;
    }

    public void setSblx(String sblx) {
        this.sblx = sblx;
    }

    public String getGxdw() {
        return gxdw;
    }

    public void setGxdw(String gxdw) {
        this.gxdw = gxdw;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public List<WjsBean> getWjs() {
        return wjs;
    }

    public void setWjs(List<WjsBean> wjs) {
        this.wjs = wjs;
    }

    public String getZj() {
        return zj;
    }

    public void setZj(String zj) {
        this.zj = zj;
    }
}
