package com.hsapp.bean;

/**
 * Created by lc on 2017/11/28.
 */
public class CSBean {
    private String mc;
    private String zj;

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getZj() {
        return zj;
    }

    public void setZj(String zj) {
        this.zj = zj;
    }
}
