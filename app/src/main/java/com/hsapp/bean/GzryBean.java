package com.hsapp.bean;

/**
 * Created by apple on 2018/1/31.
 */

public class GzryBean {
    private String jlsj;
    private String gzsj;
    private String sfzh;
    private String sbmc;
    private String sbl;
    private String sbdz;
    private String zj;

    private GZRYDataBean gzry;

    public String getJlsj() {
        return jlsj;
    }

    public void setJlsj(String jlsj) {
        this.jlsj = jlsj;
    }


    public String getSfzh() {
        return sfzh;
    }

    public void setSfzh(String sfzh) {
        this.sfzh = sfzh;
    }

    public String getSbmc() {
        return sbmc;
    }

    public void setSbmc(String sbmc) {
        this.sbmc = sbmc;
    }

    public String getSbl() {
        return sbl;
    }

    public void setSbl(String sbl) {
        this.sbl = sbl;
    }

    public String getSbdz() {
        return sbdz;
    }

    public void setSbdz(String sbdz) {
        this.sbdz = sbdz;
    }

    public GZRYDataBean getGzry() {
        return gzry;
    }

    public void setGzry(GZRYDataBean gzry) {
        this.gzry = gzry;
    }

    public String getZj() {
        return zj;
    }

    public void setZj(String zj) {
        this.zj = zj;
    }

    public String getGzsj() {
        return gzsj;
    }

    public void setGzsj(String gzsj) {
        this.gzsj = gzsj;
    }

}
