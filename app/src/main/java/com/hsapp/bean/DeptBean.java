package com.hsapp.bean;

/**
 * Created by apple on 2018/1/25.
 */

public class DeptBean {
    private String code;
    private DeptDataBean data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DeptDataBean getData() {
        return data;
    }

    public void setData(DeptDataBean data) {
        this.data = data;
    }
}
