package com.hsapp.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by apple on 2018/2/22.
 */

public class GZRYDataBean implements Serializable {
    private String jlsj;//记录时间
    private String xm;
    private String sfzh;
    private String hjdz;//户籍地址
    private String jyaq;//简要案情
    private String lytext;//人员类型
    private List<WjsBean> wjs;//照片

    public String getJlsj() {
        return jlsj;
    }

    public void setJlsj(String jlsj) {
        this.jlsj = jlsj;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getSfzh() {
        return sfzh;
    }

    public void setSfzh(String sfzh) {
        this.sfzh = sfzh;
    }

    public String getHjdz() {
        return hjdz;
    }

    public void setHjdz(String hjdz) {
        this.hjdz = hjdz;
    }

    public String getJyaq() {
        return jyaq;
    }

    public void setJyaq(String jyaq) {
        this.jyaq = jyaq;
    }

    public String getLytext() {
        return lytext;
    }

    public void setLytext(String lytext) {
        this.lytext = lytext;
    }

    public List<WjsBean> getWjs() {
        return wjs;
    }

    public void setWjs(List<WjsBean> wjs) {
        this.wjs = wjs;
    }
}
