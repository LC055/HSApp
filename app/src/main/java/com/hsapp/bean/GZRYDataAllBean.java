package com.hsapp.bean;

import java.util.List;

/**
 * Created by apple on 2018/3/12.
 */

public class GZRYDataAllBean {
    private GZRYDataBean gzry;
    private List<WjsBean> wjs;

    public GZRYDataBean getGzry() {
        return gzry;
    }

    public void setGzry(GZRYDataBean gzry) {
        this.gzry = gzry;
    }

    public List<WjsBean> getWjs() {
        return wjs;
    }

    public void setWjs(List<WjsBean> wjs) {
        this.wjs = wjs;
    }
}
