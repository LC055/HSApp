package com.hsapp.bean;

import java.util.List;

/**
 * Created by apple on 2018/3/7.
 */

public class SBListBean {
    private String total;
    private List<SBDetailBean> rows;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<SBDetailBean> getRows() {
        return rows;
    }

    public void setRows(List<SBDetailBean> rows) {
        this.rows = rows;
    }
}
