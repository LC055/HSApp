package com.hsapp.util;

import android.content.Context;
import android.widget.Toast;

import com.hsapp.bean.GWBean;

/**
 * Created by lc on 2017/11/24.
 */
public class Static {

    public static final String imgurl="http://127.0.0.1:2090/hsznfk/upload/";//D536C5DF95EF4FAC9C08F76E01825863.jpg

    public static final int STATE_RESET = -1;
    public static final int STATE_PREPARE = 0;
    public static final int STATE_BEGIN = 1;
    public static final int STATE_FINISH = 2;

    public static final String rows = "10";
    public static GWBean GWDetail=null;

    public static String NET_ERROR="网络连接失败";//无网络
    public static String DATA_ERROR="数据有误";
    public static String DATA_EMPTY="暂无更多数据";
    public static String TIMEEND="已反馈";
    public static String UPERROR="上传失败";

    public static String policecode="028751";//"026343";//"33120540";//028751
    public static String sfzh="330211196806175015";//"330211196806175015";
    public static String id="63FDFF87B00000C0E0530A7701B200C0";



    public static String SERVER="http://127.0.0.1:2090/hsznfk/phoneApi";
    public static String CSLIST=SERVER+"/allList_api";//场所列表 gxdwdm
    public static String SAVEFILE=SERVER+"/twzb_api";//保存
    public static String PCSLIST=SERVER+"/allDept_api";//派出所列表
    public static String DEPTCODE=SERVER+"/getDeptCode_api";//警号对应的派出所
    public static String GWList=SERVER+"/msgList";//
    public static String FKURL=SERVER+"/resultMsg";//反馈
    public static String GZRYLIST=SERVER+"/gzrylist";//关注人员列表
    public static String GZDETAIL=SERVER+"/gzryxq";//关注的详情 zj
    public static String SBLIST=SERVER+"/cjsblist";//设备list fPolice_No _page _row sbmc
    public static String UPIMAGE=SERVER+"/uploadCjsb";//上传图片
    public static String SAVESB=SERVER+"/save";//保存
    public static String DelImg=SERVER+"/delCjsbPic";//删除设备照片 zj



    public static void showToast(Context context,String msg){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }
}
