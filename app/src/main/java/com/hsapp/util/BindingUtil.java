package com.hsapp.util;

import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by nbzl on 2017/8/8.
 */
public class BindingUtil {

    public static String isEmpty(String label, String content) {
        if (content == null || content.equals("")) {
            return label + "无";
        } else {
            return label + content;
        }
    }

    public static String isEmpty(String content) {
        if (content == null || content.equals("")) {
            return "";
        } else {
            return content;
        }
    }

    public static String checkFk(String check) {

        if (check.equals("0")) {
            return "核查";
        } else if (check.equals("1")) {
            return "再核查";
        } else {
            return "";
        }
    }

    public static boolean checkFkStatus(String check) {
        if (check.equals("0")) {
            return false;
        } else {
            return true;
        }
    }

    public static String isTextViewEmpty(TextView obj) {
        if (obj.getText() == null) {
            return "";
        } else {
            return obj.getText().toString();
        }
    }

    public static String isTextViewEmpty(EditText obj) {
        if (obj.getText() == null) {
            return "";
        } else {
            return obj.getText().toString();
        }
    }

    public static boolean isGps(String str) {
        if (str == null || str.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean sjType(String type) {
        if (type.equals("YQJS")) {
            return true;
        } else {
            return false;
        }
    }

    public static String utilSex(int sex) {
        if (sex == 0) {
            return "女";
        } else {
            return "男";
        }
    }

    public static String utilTime(String time, String fkzt) {

        Log.e("Mytext", "time-->" + time + "   fkzt--->" + fkzt);
        int h = Integer.valueOf(time) / 1000 / 3600;
        int m = (Integer.valueOf(time) / 1000 - h * 3600) / 60;
        int s = Integer.valueOf(time) / 1000 - m * 60 - h * 3600;
        if (h == 0 && m == 0 && s == 0) {
            if (fkzt == null) {
                return "";
            } else if (fkzt.equals("0")) {
                return "到时未反馈";
            } else if (fkzt.equals("1")) {
                return "到时已反馈";
            }
            return Static.TIMEEND;
        } else {
            return h + ":" + m + ":" + s;
        }


    }

    public static boolean utilBoolTime(String time) {
        if (time.equals(Static.TIMEEND) || time.equals("0")) {
            return false;
        } else {
            return true;
        }
    }

    public static String utilSBType(String type){
        if(type.equals("--＞")){
            return "无";
        }else{
            return type;
        }
    }
}
